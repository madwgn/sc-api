import os, subprocess


def backup_vm(name,uuid,exp):
    cmd = f'printf "%s\n" "{name}" "{uuid}" "{exp}" | vm.sh'
    a = subprocess.check_output(cmd, shell=True).decode("utf-8")
    return a

def backup_tr(name,uuid,exp):
    cmd = f'printf "%s\n" "{name}" "{uuid}" "{exp}" | tr.sh'
    a = subprocess.check_output(cmd, shell=True).decode("utf-8")
    return a



def on_cadang():
    cmd = f'cadang.sh'
    a = subprocess.check_output(cmd, shell=True).decode("utf-8")
    return a



def off_cadang():
    cmd = f'delcadang.sh'
    a = subprocess.check_output(cmd, shell=True).decode("utf-8")
    return a





#
# if __name__ == "__main__":
#     print(backup_vm("whsvsv","07fd3c31-9f56-4644-b34d-7eba41f34c54","2024-04-10"))
