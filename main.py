from fastapi import *
import trial
import create
import renew
import backup
import uvicorn


from fastapi import FastAPI, Request
from pydantic import BaseModel
import uvicorn
import create
import subprocess

import aiofiles

from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel
import jwt
from jwt.exceptions import DecodeError


# Secret key untuk JWT (sebaiknya simpan dengan aman, jangan letakkan di sini)
SECRET_KEY = "madewgn"
ALGORITHM = "HS256"

# Model untuk JWT token
class Token(BaseModel):
    access_token: str
    token_type: str

# Mekanisme otentikasi
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

# Fungsi untuk memverifikasi token JWT
def verify_token(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        # Di sini Anda dapat menambahkan logika untuk memeriksa dan mengizinkan akses berdasarkan payload token yang diterima
        return payload
    except DecodeError:
        raise HTTPException(status_code=401, detail="Token tidak valid")



app = FastAPI()

# Model Pydantic untuk data yang diharapkan
class UserData(BaseModel):
    username: str
    rebug: str
    expired: int

class sssh(BaseModel):
    username: str
    pw: str
    expired: int

class cadang(BaseModel):
    username: str
    uuid: str
    expired: str


class PerPanjang(BaseModel):
    username: str
    expired: int


@app.get("/protected")
async def protected_route(token: str = Depends(verify_token)):
    return {"pesan": "Akses berhasil!"}


@app.get("/restart")
async def restart_server():
    cmd = f'restart'
    subprocess.check_output(cmd,shell=True)    
    return {"pesan": "restart berhasil!"}


@app.get("/reboot")
async def reboot_server():
    cmd = f'reboot'
    subprocess.check_output(cmd,shell=True)    
    return {"pesan": "reboot berhasil!"}


@app.get("/")
async def indek():
    return {"status": "Running"}

async def check_file_status():
    try:
        async with aiofiles.open('/etc/cadang.txt', 'r') as file:
            content = await file.read()
            content = content.strip()
            if content == 'On':
                return "aktif"
            elif content == 'Off':
                return "tidak"
            else:
                return "status tidak diketahui"
    except FileNotFoundError:
        raise HTTPException(status_code=404, detail="File tidak ditemukan")



@app.get("/cadang-on")
async def cadang_on():
    cek = await check_file_status()
    if cek == 'aktif':
        return 'akun backup sudah aktif'
    else:
        return backup.on_cadang()



@app.get("/cadang-off")
async def cadang_off():
    cek = await check_file_status()
    if cek == 'tidak':
        return 'akun backup sudah tidak aktif'
    else:
        return backup.off_cadang()







@app.post("/backup/vmess")
async def bckp_vm(request: Request, user_data: cadang):
    return backup.backup_vm(user_data.username,user_data.uuid,user_data.expired)

@app.post("/backup/trojan")
async def bckp_tr(request: Request, user_data: cadang):
    return backup.backup_tr(user_data.username,user_data.uuid,user_data.expired)




@app.post("/vps/renewssh")
async def renew_ssh(request: Request, user_data: PerPanjang):
    return renew.ssh(user_data.username, user_data.expired)

@app.post("/vps/renewtrojan")
async def renew_tr(request: Request, user_data: PerPanjang):
    return renew.trojan(user_data.username, user_data.expired)


@app.post("/vps/renewvmess")
async def renew_vm(request: Request, user_data: PerPanjang):
    return renew.vmess(user_data.username, user_data.expired)


@app.post("/vps/renewvless")
async def renew_vl(request: Request, user_data: PerPanjang):
    return create.trojan(user_data.username, user_data.expired)

@app.get("/up")
async def up_sc():
    cmd = ''' 
cd /root
wget -q https://sc.madewgn.my.id/update.sh
chmod +x update.sh
./update.sh    
    '''
    subprocess.check_output(cmd, shell=True).decode("utf-8")
    #    subprocess.check_output('systemctl restart wgnapi', shell=True).decode("utf-8")
    return "berhasil di update"




@app.get("/update")
async def up():
    subprocess.check_output('cd /etc/api && git pull', shell=True).decode("utf-8")
    #    subprocess.check_output('systemctl restart wgnapi', shell=True).decode("utf-8")
    return "berhasil di update"


@app.post("/vps/trialvlessws")
async def index():
    return trial.trial_vl()


@app.post("/vps/trialtrojanws")
async def trialtr():
    return trial.trial_tr()

    

@app.post("/vps/trialvmessws")
async def trialws():
    return trial.trial_vm()



@app.post("/vps/trialsshvpn")
async def trialssh():
    return trial.trial_ssh()


# create

@app.post("/vps/sshvpn")
async def sshv(request: Request, data: sssh):
    return create.ssh(data.username,data.pw,data.expired)


@app.post("/vps/trojanws")
async def tr(request: Request, user_data: UserData):
    return create.trojan(user_data.username, user_data.expired)

@app.post("/vps/vmessws")
async def vm(request: Request, user_data: UserData):
    return create.vmess(user_data.username, user_data.expired)

@app.post("/vps/vlessws")
async def vl(request: Request, user_data: UserData):
    return create.vless(user_data.username, user_data.expired)



if __name__ == "__main__":
   uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)

