import requests

serv = ['au-azure.madewgn.eu.org','idc.madewgn.eu.org','do.madewgn.eu.org','do2.madewgn.eu.org']


for i in serv:
    url = f'http://{i}:8000/up'
    response = requests.get(url)
    print(f'Response from {i}:8000/up:')
    print(response.text)


